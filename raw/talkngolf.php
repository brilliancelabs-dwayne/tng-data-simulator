<?php

class talkngolf {
   function __construct($x=null,$y=null,$z=null,$aa=null) {
      @talkngolf::debug($x,$y,$z,$aa);
   }
   
   public function debug ($x = null, $bg = 'gold', $color = 'black', $weight = 'normal') {
      if($x):
         if(!$bg) $bg = 'gold';
         if(!$color) $color = 'black';
         if(!$weight) $weight = 'normal';


         $div_open = "<div class='debug' style='padding:15px;margin-bottom:5px;background-color:%s;color:%s;font-weight:%s'><pre>";
         $div_close = "</pre></div>";

         printf($div_open, $bg, $color, $weight);

         //Prefix
         $prefix = "";

         print $prefix;
         if(is_array($x)):

            print_r($x);	

         else:

            print $x;	

         endif;

         print $div_close;
      endif;

	}
	
   public function load_course($course = 'BethPage'){
		$courses = array(
			'BethPage' => array(
				'Blue' => array(
					'info' => array(),
					'holes' => array(
						1 => array(
							'par' => 4,
						),
						2 => array(
							'par' => 4,
						),
						3 => array(
							'par' => 3,
						),
						4 => array(
							'par' => 5,
						),
						5 => array(
							'par' => 4,
						),
						6 => array(
							'par' => 4,
						),
						7 => array(
							'par' => 3,
						),
						8 => array(
							'par' => 5,
						),
						9 => array(
							'par' => 4,
						),
						10 => array(
							'par' => 4,
						),
						11 => array(
							'par' => 3,
						),
						12 => array(
							'par' => 5,
						),
						13 => array(
							'par' => 4,
						),
						14 => array(
							'par' => 4,
						),
						15 => array(
							'par' => 4,
						),
						16 => array(
							'par' => 5,
						),
						17 => array(
							'par' => 3,
						),
						18 => array(
							'par' => 4,
						),
					)

				)
			)

		);//end course list

		return $courses;

	}

	public function get_score($hole = 4){

		switch($hole):	

			case 3:
				$score = rand(1,6);
			break;

			case 4:
				$score = rand(2,8);
			break;

			case 5:
				$score = rand(2,10);
			break;

			default;
			//do nothing

		endswitch;

		return $score;

	}
	
	public function get_club(){
		//Needs logical restrictions and correlations
		return rand(1,13);
	}
	
   public function get_mood(){
		//Needs logical restrictions and correlations
		return rand(1,4);
	}
	
   public function get_shots($score = null){
		if($score):
			for($i=1;$i<$score;$i++):
				//Set a club and mood for each shot
				$shots[$i] = array(
					'club' => $this->get_club(),
					'mood' => $this->get_mood() 
				);
			endfor;
			
			//The last shot for this simulation will always be a putter
				//$score is max of array
				//13 represents putter
			$shots[$score] = array(
				'club' => 13,
				'mood' => $this->get_mood()

			);

			return $shots;

		endif;
		
	}

	public function play_round($course = array()){
		//Get course data from database or file
		$course=$this->load_course()['BethPage']['Blue']['holes'];

		//Get a stroke score for each hole in the round
		$hole = 1;		
		foreach($course as $par):
			//Get simulated score						
			$score = $this->get_score($par['par']);

			//Simulte shot data for a particular score
			$shots = $this->get_shots($score);

			//Fill scorecard array
			$tally[$hole] = array(
				'hole'=> $hole,
				'par' => $par['par'],
				'score' => $score,
				'overunder' => $score-$par['par'],
				'shots' => $shots,
			);

			//Keep separate value for easy round totalling
			$total[]=$score;

			$hole++;
		endforeach;
		
		//Round total
		$round = array_sum($total);
      
      //Restrict round to a reasonable score for a moderate golfer
      if($round < 111 && $round > 87):

         $plusminus = $round-72;

         $tally[0] = array(
              'Score' => $round,
              '+/-' => sprintf("%+d",$plusminus) //add a sign to the plusminus number
         );

         return $tally;
      
      else:
         //Try again
         return talkngolf::play_round();
         
         //Return nothing
         #return array();
      
      endif;
	}

   public function automate_rounds($num = 1){
      while($num):
         $rounds[]=$this->play_round();
         //decrement the provided number
         $num--;
      endwhile;
      
      //return the results in an array
      return $rounds;
   }
   
  public function db_connect(){
     $host = "127.0.0.1";
     $user = "root";
     $pass = "";
     $conn = mysql_connect($host,$user,$pass) or die("No connection");
     if($conn):
      mysql_select_db("tng_data", $conn) or die("DB error");
     endif;
  }
  public function db_insert($table = null, $field = null, $where = null, $condition_value){
      $this->db_connect();
      
      if(is_array($field)):
         
         $field_sql = "(";
         $value_sql = " VALUES (";
         foreach($field as $k=>$v):
            $field_sql.=$k.", ";
            $value_sql.="\"".$v."\", ";
         endforeach;
         $field_sql = rtrim($field_sql,", ");
         $value_sql = rtrim($value_sql,", ");
         
         $field_sql .= ") ";
         $value_sql .= ") ";
      else:
         echo $field;
      endif;
      
      $sql_fv = $value_sql;
      
      $query = "INSERT INTO %s %s;";
      
      if($where):
         $query.= " WHERE %s = '%s';";
         $sql = sprintf($query,$table,$sql_fv,$where,$condition_value);
      
      else:
         
         $sql = sprintf($query,$table,$sql_fv);
      endif;
      
      print $sql;
      msyql_query($sql) or mysql_error();
      mysql_close();
  } 
   
   
   
   
   
   
}//class dismissed

//Create an instance of the class
$tng = new talkngolf;
