create database tng_data;
use tng_data;
create table if not exists rounds (
   id int not null auto_increment primary key,
   Score double(2,0) not null,
   PlusMinus int(2) signed not null,
   Mood double(1,0)
);
create table if not exists round_data (
   id bigint not null auto_increment primary key,
   round_id int not null,
   Hole int(2) not null,
   Stroke int(2) not null,
   Club int(2) not null,
   Mood int(2) not null
);
