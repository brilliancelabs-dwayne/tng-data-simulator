/**
*TalknGolf Data Simulator
**/
create table if not exists courses (
   id int not null auto_increment primary key,
   FacilityName varchar(75) not null,
   CourseName varchar(75) not null,
   Par double(2,0) not null,
   Slope double(2,0),
   City varchar(75),
   State char(2),
   Zip varchar(15)
);
create table if not exists brands (
   id int not null auto_increment primary key,
   Name varchar(75) not null
);
create table if not exists players (
   id int not null auto_increment primary key,
   username varchar(75) unique,
   Name varchar(150)
);
create table if not exists club_sets (
   id int not null auto_increment primary key,
   player_id int not null,
   Name varchar(75) not null,
   brand_id int not null,
   ModelName varchar(75),
   MakeName varchar(75),
   MakeYear year(4)
);
create table if not exists clubs (
   id int not null auto_increment primary key,
   club_set_id int not null,
   ClubNumber tinyint(2) not null,
   Name varchar(75) not null,
   Active char(1) default '1',
   Created date,
   Updated timestamp default current_timestamp() on update current_timestamp()
);
create table if not exists rounds (
   id int not null auto_increment primary key,
   player_id not null,
   course_id not null,
   Score double(2,0) not null,
   PlusMinus int(2) signed not null,
   Mood double(1,0)
);
create table if not exists strokes (
   id bigint not null auto_increment primary key,
   round_id int not null,
   club_id int not null,
   Hole int(2) not null,
   Mood double(1,0),
   Comment longtext
);