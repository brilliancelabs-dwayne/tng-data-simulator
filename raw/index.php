<?php require_once "talkngolf.php";?>

<style type="text/css">
table {
	width: 50%;
	border: solid thin black;
}
td,th {
	text-align: center;
	padding: 10px;
	border: dashed thin #CCC;
}
th {
	background: #EEE;
}
</style>
<h2>TalknGolf</h2>

<h3>Score Card</h3>
<table>
<col width="20%" style="background-color:skyblue" />
<thead>
	<tr>
		<th>Hole</th><th>Par</th><th>Score</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>1</td><td>4</td><td>1</td>
	</tr>
</tbody>
</table>

<hr />
<h3>Beth Page Blue Course</h3>
<h4>Mood Card</h4>
<table>
<col width="20%" style="background-color:skyblue" />
<thead>
	<tr>
		<th>Hole</th><th>Par</th><th>Club</th><th>Stroke</th><th>Mood</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>1</td><td>4</td><td>1</td><td>1</td><td>3</td>
	</tr>
</tbody>
</table>

<hr />
<h3>Beth Page Blue Course</h3>
<h4>Mood Card</h4>
<table>
<col width="20%" style="background-color:skyblue" />
<thead>
   <tr>
      <th>Score</th>
      <th><?php echo $round[0]['Score'];?></th>
      <th><?php echo $round[0]['+/-'];?></th>
   </tr>
	<tr>
      <th>Hole</th>
      <th>Stroke</th>
      <th>Mood</th>

	</tr>
</thead>
<tbody>
<?php foreach($round as $r):
?>

   <?php foreach($r['shots'] as $stroke => $shot):?>
	<tr>
      <td><?php echo $r['hole'];?></td>
      <td><?php echo $stroke;?></td>
      <td><?php echo $shot['mood'];?></td>
	</tr>
   <?php endforeach;?>
<?php endforeach;?>
</tbody>
</table>