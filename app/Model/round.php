<?php
App::uses('AppModel', 'Model');
/**
 * round Model
 *
 * @property RoundDatum $RoundDatum
 */
class round extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'PlusMinus' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'RoundDatum' => array(
			'className' => 'RoundDatum',
			'foreignKey' => 'round_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
