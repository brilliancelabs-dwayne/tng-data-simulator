<div class="rounds form">
<?php echo $this->Form->create('Round'); ?>
	<fieldset>
		<legend><?php echo __('Edit Round'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('Score');
		echo $this->Form->input('PlusMinus');
		echo $this->Form->input('Mood');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Round.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Round.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Rounds'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Round Data'), array('controller' => 'round_data', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round Datum'), array('controller' => 'round_data', 'action' => 'add')); ?> </li>
	</ul>
</div>
