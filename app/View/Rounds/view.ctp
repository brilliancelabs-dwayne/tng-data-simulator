<div class="rounds view">
<h2><?php  echo __('Round'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($round['Round']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Score'); ?></dt>
		<dd>
			<?php echo h($round['Round']['Score']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PlusMinus'); ?></dt>
		<dd>
			<?php echo h($round['Round']['PlusMinus']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mood'); ?></dt>
		<dd>
			<?php echo h($round['Round']['Mood']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Round'), array('action' => 'edit', $round['Round']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Round'), array('action' => 'delete', $round['Round']['id']), null, __('Are you sure you want to delete # %s?', $round['Round']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Rounds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Round Data'), array('controller' => 'round_data', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round Datum'), array('controller' => 'round_data', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Round Data'); ?></h3>
	<?php if (!empty($round['RoundDatum'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Round Id'); ?></th>
		<th><?php echo __('Hole'); ?></th>
		<th><?php echo __('Stroke'); ?></th>
		<th><?php echo __('Club'); ?></th>
		<th><?php echo __('Mood'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($round['RoundDatum'] as $roundDatum): ?>
		<tr>
			<td><?php echo $roundDatum['id']; ?></td>
			<td><?php echo $roundDatum['round_id']; ?></td>
			<td><?php echo $roundDatum['Hole']; ?></td>
			<td><?php echo $roundDatum['Stroke']; ?></td>
			<td><?php echo $roundDatum['Club']; ?></td>
			<td><?php echo $roundDatum['Mood']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'round_data', 'action' => 'view', $roundDatum['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'round_data', 'action' => 'edit', $roundDatum['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'round_data', 'action' => 'delete', $roundDatum['id']), null, __('Are you sure you want to delete # %s?', $roundDatum['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Round Datum'), array('controller' => 'round_data', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
