<div class="roundData view">
<h2><?php  echo __('Round Datum'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($roundDatum['RoundDatum']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Round'); ?></dt>
		<dd>
			<?php echo $this->Html->link($roundDatum['Round']['id'], array('controller' => 'rounds', 'action' => 'view', $roundDatum['Round']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hole'); ?></dt>
		<dd>
			<?php echo h($roundDatum['RoundDatum']['Hole']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Stroke'); ?></dt>
		<dd>
			<?php echo h($roundDatum['RoundDatum']['Stroke']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Club'); ?></dt>
		<dd>
			<?php echo h($roundDatum['RoundDatum']['Club']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mood'); ?></dt>
		<dd>
			<?php echo h($roundDatum['RoundDatum']['Mood']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Round Datum'), array('action' => 'edit', $roundDatum['RoundDatum']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Round Datum'), array('action' => 'delete', $roundDatum['RoundDatum']['id']), null, __('Are you sure you want to delete # %s?', $roundDatum['RoundDatum']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Round Data'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round Datum'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rounds'), array('controller' => 'rounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round'), array('controller' => 'rounds', 'action' => 'add')); ?> </li>
	</ul>
</div>
