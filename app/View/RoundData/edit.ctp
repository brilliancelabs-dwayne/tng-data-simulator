<div class="roundData form">
<?php echo $this->Form->create('RoundDatum'); ?>
	<fieldset>
		<legend><?php echo __('Edit Round Datum'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('round_id');
		echo $this->Form->input('Hole');
		echo $this->Form->input('Stroke');
		echo $this->Form->input('Club');
		echo $this->Form->input('Mood');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RoundDatum.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('RoundDatum.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Round Data'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Rounds'), array('controller' => 'rounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round'), array('controller' => 'rounds', 'action' => 'add')); ?> </li>
	</ul>
</div>
