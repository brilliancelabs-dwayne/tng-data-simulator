<div class="roundData index">
	<h2><?php echo __('Round Data'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('round_id'); ?></th>
			<th><?php echo $this->Paginator->sort('Hole'); ?></th>
			<th><?php echo $this->Paginator->sort('Stroke'); ?></th>
			<th><?php echo $this->Paginator->sort('Club'); ?></th>
			<th><?php echo $this->Paginator->sort('Mood'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($roundData as $roundDatum): ?>
	<tr>
		<td><?php echo h($roundDatum['RoundDatum']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($roundDatum['Round']['id'], array('controller' => 'rounds', 'action' => 'view', $roundDatum['Round']['id'])); ?>
		</td>
		<td><?php echo h($roundDatum['RoundDatum']['Hole']); ?>&nbsp;</td>
		<td><?php echo h($roundDatum['RoundDatum']['Stroke']); ?>&nbsp;</td>
		<td><?php echo h($roundDatum['RoundDatum']['Club']); ?>&nbsp;</td>
		<td><?php echo h($roundDatum['RoundDatum']['Mood']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $roundDatum['RoundDatum']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $roundDatum['RoundDatum']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $roundDatum['RoundDatum']['id']), null, __('Are you sure you want to delete # %s?', $roundDatum['RoundDatum']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Round Datum'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Rounds'), array('controller' => 'rounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round'), array('controller' => 'rounds', 'action' => 'add')); ?> </li>
	</ul>
</div>
