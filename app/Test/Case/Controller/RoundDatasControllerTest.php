<?php
App::uses('RoundDatasController', 'Controller');

/**
 * RoundDatasController Test Case
 *
 */
class RoundDatasControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.round_data'
	);

}
