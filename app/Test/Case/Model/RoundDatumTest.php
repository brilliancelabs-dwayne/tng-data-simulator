<?php
App::uses('RoundDatum', 'Model');

/**
 * RoundDatum Test Case
 *
 */
class RoundDatumTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.round_datum',
		'app.round'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RoundDatum = ClassRegistry::init('RoundDatum');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RoundDatum);

		parent::tearDown();
	}

}
