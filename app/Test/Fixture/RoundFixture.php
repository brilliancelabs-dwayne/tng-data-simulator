<?php
/**
 * RoundFixture
 *
 */
class RoundFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'Score' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => 2),
		'PlusMinus' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2),
		'Mood' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => 1),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'Score' => 1,
			'PlusMinus' => 1,
			'Mood' => 1
		),
	);

}
