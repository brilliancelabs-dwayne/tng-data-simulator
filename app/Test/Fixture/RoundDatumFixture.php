<?php
/**
 * RoundDatumFixture
 *
 */
class RoundDatumFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'key' => 'primary'),
		'round_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'Hole' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2),
		'Stroke' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2),
		'Club' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2),
		'Mood' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'round_id' => 1,
			'Hole' => 1,
			'Stroke' => 1,
			'Club' => 1,
			'Mood' => 1
		),
	);

}
