<?php
App::uses('AppController', 'Controller');
/**
 * Rounds Controller
 *
 * @property Round $Round
 */
class RoundsController extends AppController {

   public function play($iterations = 2){

      $this->autoRender = false;

      print("Simulating ".$iterations." Rounds");


      foreach($this->automate_rounds($iterations) as $rounds):

         //Prints results to page though this data would normally pass through
         ///to an application such as Mathematica
         debug($rounds);

      endforeach;

   } 
   

   public function load_course($course = 'BethPage'){
		$courses = array(
			'BethPage' => array(
				'Blue' => array(
					'info' => array(),
					'holes' => array(
						1 => array(
							'par' => 4,
						),
						2 => array(
							'par' => 4,
						),
						3 => array(
							'par' => 3,
						),
						4 => array(
							'par' => 5,
						),
						5 => array(
							'par' => 4,
						),
						6 => array(
							'par' => 4,
						),
						7 => array(
							'par' => 3,
						),
						8 => array(
							'par' => 5,
						),
						9 => array(
							'par' => 4,
						),
						10 => array(
							'par' => 4,
						),
						11 => array(
							'par' => 3,
						),
						12 => array(
							'par' => 5,
						),
						13 => array(
							'par' => 4,
						),
						14 => array(
							'par' => 4,
						),
						15 => array(
							'par' => 4,
						),
						16 => array(
							'par' => 5,
						),
						17 => array(
							'par' => 3,
						),
						18 => array(
							'par' => 4,
						),
					)

				)
			)

		);//end course list

		return $courses;

	}

   //Simulates an actual golf score based on the par for the hole
   ///Maximum score is 2x the par (stroke limit applied)
   ///Minimum score is a hole-in-one for par 3's
	public function get_score($par = 4){

		switch($par):	

			case 3:
				$score = rand(1,6); //hole in one possible
			break;

			case 4:
				$score = rand(2,8); //2-stroke min; hole in one not possible
			break;

			case 5:
				$score = rand(2,10);
			break;

			default;
            //should never happen
			   $score = 0;

		endswitch;

		return $score;

	}
	
	public function get_club(){
		//Needs logical restrictions and correlations
      ///Logical restrictions do not exist now
      ///Club 1 is Driver and 13 is putter
		return rand(1,13);
	}
	
   public function get_mood(){
		//Needs logical restrictions and correlations
      ///'1' is TERRIBLE SHOT '4' is AWESOME SHOT
		return rand(1,4);
	}
	
   public function get_shots($score = null){
		if($score):
			for($i=1;$i<$score;$i++):
				//Set a club and mood for each shot
				$shots[$i] = array(
					'club' => $this->get_club(),
					'mood' => $this->get_mood() 
				);
			endfor;
			
			//The last shot for this simulation will always be a putter
				//$score is max of array
				//#13 represents putter
			$shots[$score] = array(
				'club' => 13,
				'mood' => $this->get_mood()

			);

			return $shots;

		endif;
		
	}
   
   public function play_round($course = array()){

      //Get course data from database or file
      $course=$this->load_course();

      //Harcoding course for this particular app      
      $course=$course['BethPage']['Blue']['holes'];

      //Get a stroke score for each hole in the round
      $hole = 1;		
      foreach($course as $par):
         //Get simulated score						
         $score = $this->get_score($par['par']);

         //Simulte shot data for a particular score
         $shots = $this->get_shots($score);

         //Fill scorecard array
         $tally[$hole] = array(
            'hole'=> $hole,
            'par' => $par['par'],
            'score' => $score,
            'overunder' => $score-$par['par'],
            'shots' => $shots,
         );

         //Keep separate value for easy round totalling
         $total[]=$score;

         $hole++;

      endforeach;

      //Round total
      $round = array_sum($total);
      
      $low = 87;
      $high = 111;
      $range = $high-$low;
      
      /***************************************************/
      $range_1 = $low+$range*.15;
      $range_2 = $low+$range*.40;
      $range_3 = $low+$range*.65;
      $range_4 = $low+$range*.90;
      
      
      if($score<$range_1):

         $round_mood = 4;

      elseif($score<$range_2):

         $round_mood = 3;

      elseif($score<$range_3):

         $round_mood = 2;

      else:

         $round_mood = 1;

      endif;
      

      //Restrict round to a reasonable score for a moderate golfer
      if($round > $low && $round < $high):

         $plusminus = $round-72;

         //Round Mood
         $tally[0] = array(
              'Score' => $round,
              '+/-' => sprintf("%+d",$plusminus), //add a sign to the plusminus number
              'Mood' => 1
         );

         return $tally;

      else:

         //Try again?
         ///This recursion may cause crashing
         ///PHP doesn't seem to like recursion
         return RoundsController::play_round();

         //Return nothing
         ///This is the safe option
         #return array();

      endif;
   }

   public function automate_rounds($num = 1){

      while($num):

         $rounds[]=$this->play_round();

         $num--;

      endwhile;

      //return the results in an array
      return $rounds;

   } 

   




   
   
/** THE FOLLOWING METHODS WERE SCAFFOLDED
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Round->recursive = 0;
		$this->set('rounds', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Round->exists($id)) {
			throw new NotFoundException(__('Invalid round'));
		}
		$options = array('conditions' => array('Round.' . $this->Round->primaryKey => $id));
		$this->set('round', $this->Round->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Round->create();
			if ($this->Round->save($this->request->data)) {
				$this->Session->setFlash(__('The round has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The round could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Round->exists($id)) {
			throw new NotFoundException(__('Invalid round'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Round->save($this->request->data)) {
				$this->Session->setFlash(__('The round has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The round could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Round.' . $this->Round->primaryKey => $id));
			$this->request->data = $this->Round->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Round->id = $id;
		if (!$this->Round->exists()) {
			throw new NotFoundException(__('Invalid round'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Round->delete()) {
			$this->Session->setFlash(__('Round deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Round was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
