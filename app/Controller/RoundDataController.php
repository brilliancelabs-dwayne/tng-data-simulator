<?php
App::uses('AppController', 'Controller');
/**
 * RoundData Controller
 *
 * @property RoundDatum $RoundDatum
 */
class RoundDataController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RoundDatum->recursive = 0;
		$this->set('roundData', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RoundDatum->exists($id)) {
			throw new NotFoundException(__('Invalid round datum'));
		}
		$options = array('conditions' => array('RoundDatum.' . $this->RoundDatum->primaryKey => $id));
		$this->set('roundDatum', $this->RoundDatum->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RoundDatum->create();
			if ($this->RoundDatum->save($this->request->data)) {
				$this->Session->setFlash(__('The round datum has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The round datum could not be saved. Please, try again.'));
			}
		}
		$rounds = $this->RoundDatum->Round->find('list');
		$this->set(compact('rounds'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RoundDatum->exists($id)) {
			throw new NotFoundException(__('Invalid round datum'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RoundDatum->save($this->request->data)) {
				$this->Session->setFlash(__('The round datum has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The round datum could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RoundDatum.' . $this->RoundDatum->primaryKey => $id));
			$this->request->data = $this->RoundDatum->find('first', $options);
		}
		$rounds = $this->RoundDatum->Round->find('list');
		$this->set(compact('rounds'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RoundDatum->id = $id;
		if (!$this->RoundDatum->exists()) {
			throw new NotFoundException(__('Invalid round datum'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->RoundDatum->delete()) {
			$this->Session->setFlash(__('Round datum deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Round datum was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
